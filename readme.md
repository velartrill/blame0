# blame

`blame` is a formal language for describing, precisely and succintly, exactly whose fault it is.
this is particularly useful in large, complex corporate structures and package management.
it aims to be simple, clean, easily parseable, and to accurately represent degrees of blame.
it is intended to be an enterprise-grade solution, and to leverage synergy with accounting
and directory tools.

## syntax
`blame` statements consist of the following:

	Document ::= '-' String '-' BlameStatement
	Perpetrator ::= String | Committee
	Committee ::= String '(' Perpetrator (',' Perpetrator)* ')'
	BlameStatement ::= Perpetrator ':' Degree ('←' BlameStatement)
	                 | '[' BlameStatement (',' BlameStatement) ']'

all tokens must be separated by whitespace.
	                 
`Degree` is an integer between 0 and 10 denoting how much free will the perpetrator was allowed
under corporate rules. 0 indicates that this person was simply following procedure, such as
filing a form, delivering a message, etc. 10 indicates absolute freedom, that no possible choice
regarding the failure could have resulted in their punishment under corporate rules.

`String` is an arbitrary Unicode string.

comments of the form `(* ... *)` may be inserted at any point. comments count as whitespace in terms
of lexing. they can be nested.

## example text
```
- The Bronze Doorknob Affair -
	Sam Perkins : 1 ← [
		(* that prick from Accounting *)
		Herbert Perkins : 3 ← Andrew Fernandes : 0,
		Melissa Watkins : 0 ← [
			New Developments Committee (
				Lord Jethromort Doomflower,
				Her Grace the Duchess of York
			) : 7,
			Dr Philip Renaldo : 6
		],
		Felipe Pedro Hernández : 2
	]
```
